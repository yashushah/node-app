import * as AWS from 'aws-sdk';
import * as fs from 'fs';
import { Log } from './helpers/logger';

export class Aws {
    private s3 = new AWS.S3({
        accessKeyId: process.env.S3_Bucket_accessKeyId,
        secretAccessKey: process.env.S3_Bucket_secretAccessKey,
    });
    private logger = Log.getLogger();
    // Upload to S3
    public async uploadFile(
        data: any,
        folderPath: any,
        additionalParams?
    ) {
        return new Promise(async (resolve, reject) => {
            let params;
            const epochTime = new Date().getTime();
            if (additionalParams == 'profile') {
                params = {
                    Bucket: process.env.S3_Bucket_name,
                    Key: `profile/${folderPath}/${epochTime}`,
                    Body: data.data,
                    ContentDisposition:'inline',
                    ContentType: data.mimeType,
                    ACL: 'public-read',
                };
            } else {
                params = {
                    Bucket: process.env.S3_Bucket_name,
                    Key: `uploads/${folderPath}/${epochTime}`,
                    Body: data.data,
                    ContentDisposition:'inline',
                    ContentType: data.mimeType,
                    ACL: 'public-read',
                };
            }
            await this.s3.upload(params, (err, data) => {
                if (err) {
                    this.logger.info(`File Error :- ${err}`);
                    reject(err);
                } else {
                    resolve(data);
                    this.logger.info(`File Uploaded: ${data}`);
                }
            });
        });
    }

    // Upload to S3
    public async uploadThumbFile(
        folderPath: any,
        thumbPath: any,
        fileName: any,
        additionalParams?
    ) {
        return new Promise(async (resolve, reject) => {
            const fileContent = fs.readFileSync(`${thumbPath}/${fileName}`);
            let params;
            if (additionalParams == 'profile') {
                params = {
                    Bucket: process.env.S3_Bucket_name,
                    Key: `Profile/${folderPath}/thumbs/${fileName}`,
                    Body: fileContent,
                    ACL: 'public-read',
                };
            } else {
                params = {
                    Bucket: process.env.S3_Bucket_name,
                    Key: `uploads/${folderPath}/thumbs/${fileName}`,
                    Body: fileContent,
                    ACL: 'public-read',
                };
            }
            await this.s3.upload(params, (err, data) => {
                if (err) {
                    this.logger.error(`Thumb Uploaded: ${err}`);
                    reject(err);
                } else {
                    this.logger.info(`Thumb Uploaded: ${data}`);
                    resolve(data);
                }
            });
        });
    }

    // Upload video file to S3
    public async uploadVideoThumbFile(
        folderPath: any,
        thumbPath: any,
        fileName: any
    ) {
        return new Promise(async (resolve, reject) => {
            const fileContent = fs.createReadStream(`${thumbPath}/${fileName}`);
            const params = {
                Bucket: process.env.S3_Bucket_Name,
                Key: `${folderPath}/thumbs/${fileName}`,
                Body: fileContent,
                ACL: 'public-read',
            };
            await this.s3.putObject(params, (err, data) => {
                if (err) {
                    this.logger.error(`Video Thumb Uploaded: ${err}`);
                    reject(err);
                } else {
                    this.logger.info(`Video Thumb Uploaded: ${data}`);
                    resolve(data);
                }
            });
        });
    }

    public async deleteObj(id: any) {
        const params = {
            Bucket: process.env.S3_Bucket_name,
            Key: id,
        };
        await this.s3.deleteObject(params).promise();
    }

    public async getAttachmentData(attachmentId: any) {
        const getParams = {
            Bucket: process.env.S3_Bucket_name, // your bucket name,
            Key: attachmentId, // path to the object you're looking for
        };
        await this.s3.getObject(getParams).promise();
    }
}
