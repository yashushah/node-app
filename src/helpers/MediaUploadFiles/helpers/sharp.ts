import * as fs from 'fs';
import * as path from 'path';
import * as sharp from 'sharp';
import { Log } from './logger';

export class Sharp {
    private logger = Log.getLogger();
    public async genThumbs(file, thumbs, location) {
        const ext = path.extname(file.name);
        const imageToProcess = sharp(file.data);
        const metadata = await imageToProcess.metadata();
        const fileRect = {
            height: metadata.height,
            width: metadata.width,
        };

        if (metadata.orientation && metadata.orientation >= 4) {
            // Swap hieght and width for calculation
            fileRect.height = metadata.width;
            fileRect.width = metadata.height;
        }

        const tempLocation = await this.thumbs(location, imageToProcess, thumbs, fileRect, ext);
        return tempLocation;
    }

    private asyncForEach = async (array, callback) => {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    };

    private thumbs(location, imageToProcess, thumbs, fileRect, ext) {
        return new Promise(async (resolve, reject) => {
            // Create temp location folder
            const tempLocation = `${location}/thumbs`;
            fs.mkdirSync(tempLocation);
            const promises = [];
            this.asyncForEach(thumbs, async (thumb) => {
                promises.push(this.resize(tempLocation, imageToProcess, fileRect, thumb, ext));
            });

            await Promise.all(promises);
            resolve(tempLocation);
        });
    }

    private resize(tempLocation, imageToProcess, fileRect, thumb, ext) {
        return new Promise((resolve, reject) => {
            const rect = this.getRect(fileRect, thumb);
            const thumbPath = `${tempLocation}/thumb_${thumb.id}${ext}`;
            imageToProcess
                .rotate()
                .png()
                .resize(rect.width, rect.height)
                .toFile(thumbPath, (err) => {
                    if (!err) {
                        this.logger.error(err);
                        resolve(err);
                        this.logger.info(`Thumb Generated: ${thumbPath}`);
                    } else {
                        reject(err);
                    }
                });
        });
    }

    private getRect(file, thumbRect) {
        const rect = {
            width: file.width,
            height: file.height,
        };

        if (thumbRect.centerCrop) {
            rect.width = thumbRect.width;
            rect.height = thumbRect.height;
            return rect;
        }

        if (file.width < file.height) {
            // Portrait
            rect.height = thumbRect.height;
            rect.width = Math.floor(+((thumbRect.height * file.width) / file.height));
        } else {
            // Landscape
            rect.width = thumbRect.width;
            rect.height = Math.floor(+((thumbRect.width * file.height) / file.width));
        }
        return rect;
    }
}
