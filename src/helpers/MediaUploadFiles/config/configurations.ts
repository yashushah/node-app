export class Configurations {
    public static IMAGE_THUMBS = {
        PROFILE_PICTURE: [
            {
                id: '200x200',
                height: 200,
                width: 200,
                thumbType: 'small',
            }],
        BEER_IMAGE: [
            {
                id: '200x200',
                height: 200,
                width: 50,
                thumbType: 'small',
            }],
        COMPANY_LOGO: [
            {
                id: '200x200',
                height: 200,
                width: 200,
                thumbType: 'small',
            }],
        GENERAL: [
            {
                id: '200x200',
                height: 200,
                width: 200,
                thumbType: 'small',
            }],
    };
    public static VIDEO_THUMBS = [
        {
            id: '360x240',
            height: 240,
            width: 360,
            thumbType: 'small',
        }, {
            id: '720x480',
            height: 480,
            width: 720,
            thumbType: 'medium',
        }, {
            id: '1080x720',
            height: 720,
            width: 1080,
            thumbType: 'large',
        }];

}
