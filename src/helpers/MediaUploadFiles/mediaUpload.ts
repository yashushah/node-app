import * as fs from 'fs';
import * as uuid from 'node-uuid';
import * as path from 'path';
import { Constants } from '../config/constants';
import * as _ from 'lodash';
import { Aws } from './aws';
import { Sharp } from './helpers/sharp';
import { Configurations as config } from './config/configurations';
import * as ffmpeg from 'fluent-ffmpeg';

export class Upload {
    private sharp = new Sharp();
    private aws = new Aws();
    public fileUpload = async (file: any, additionalParams?) => {
        // will upload only single file at a time

        // To store file for temperory time
        let uploadsDir = path.resolve(`${__dirname}/../../../`, 'uploads');

        //generate a unique id
        uploadsDir = `${uploadsDir}/${uuid.v1()}`;
        fs.mkdirSync(uploadsDir);
        const attachmentId = await this.processFiles(
            file,
            file.type || Constants.IMAGE_TYPE_UNDEFINED,
            uploadsDir,
            additionalParams
        );
        return attachmentId;
    };

    public async processFiles(file: any, type: string, location: string, additionalParams) {
        const { mimetype } = file;
        const tempLocation = location;
        const folderPath = tempLocation
            .substr(tempLocation.lastIndexOf(Constants.UPLOAD_FOLDER))
            .replace('uploads/', '');

        // move original file into uploads folder
        const ext = path.extname(file.name);
        const fileName = file.name.split('.')[0];
        const staticFileName = `${fileName}${ext}`;
        const data = file;
        let fileMoved = false;
        //store file into aws s3
        const savedOnAws: any = await this.aws.uploadFile(data,folderPath,additionalParams);
        const result = {
            dataPath: savedOnAws.key,
        };
        //then move image to upload folder for generate thumn file
        await data.mv(`${tempLocation}/${staticFileName}`, async (err) => {
            if (err) {
                console.log(err);
            } else {
                fileMoved = true;
                if (fileMoved) {
                    const thumbPath = `${tempLocation}/thumbs`; 
                    if (_.includes(Constants.VIDEO_MIMES, mimetype)) {
                    // To generate thumbnails from the video
                        for (let i = 0; i < config.VIDEO_THUMBS.length; i++) {
                            await this.getVideoThumbs(
                                `${tempLocation}/${staticFileName}`,
                                thumbPath,
                                i
                            );
                        }
                        const files = fs.readdirSync(thumbPath);
                        for (const eachFile of files) {
                            await this.aws.uploadVideoThumbFile(folderPath,thumbPath,eachFile);
                        }
                    }
                }
                fs.rmdirSync(tempLocation, { recursive: true });
            }
        });
        return result;
    }

    public getVideoThumbs(videoPath: string, thumbPath: string, index: number) {
        return new Promise((resolve: any, reject: any) => {
            ffmpeg(videoPath)
                .takeScreenshots({
                    count: 1,
                    size: config.VIDEO_THUMBS[index].id,
                    folder: thumbPath,
                    filename: `thumb_${config.VIDEO_THUMBS[index].id}`,
                })
                .on('end', () => {
                    resolve();
                });              
        });
    }
}
