import { Constants } from './config/constants';
import * as moment from 'moment-timezone';

export class Dates {

    public toEpoch(date){
        return (new Date(date)).getTime();
    }

    public addDays(date: Date, days: number, format: string = null) {
        const newDate = moment(date).add(days, 'days');
        newDate.set({ second: 0, millisecond: 0 });

        if (format) {
            return newDate.format(format);
        } else {
            return newDate.format();
        }
    }

    public getDateDifference(endDate: any, startDate: any = moment(), diffIn: string = Constants.DATE_DIFFERENCE_IN.DAYS) {
        const expireDate: any = moment(endDate);
        return expireDate.diff(startDate, diffIn);
    }

}
