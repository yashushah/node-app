import { Constants } from './config/constants';
import * as CryptoJS from 'crypto-js';
import * as moment from 'moment';
import { Upload } from './MediaUploadFiles/mediaUpload';
import { Dates } from './date';
import User from '../models/users';
import * as _ from 'lodash';
export class Utils {
    public static dateUtil = new Dates();
    
    public static getDateDifference = async(endDate: any, startDate: any = moment(), diffIn: string = Constants.DATE_DIFFERENCE_IN.DAYS) =>{
        const expireDate: any = moment(+endDate);
        return expireDate.diff(startDate, diffIn);
    };

    public static getUploadedMediaId = async (media: any,fileName?: string,additionalParams?: any,image?) => {
        const upload = new Upload();
        if (media) {
            const data: any = await upload.fileUpload(media, additionalParams);
            return data;
        }  
        else if(image){
            const data = {dataPath:image};
            return data;
        }
        else{
            return null;
        }
    };

    public static getImagePath = (atchName: string) => {
        return `IF(${atchName} IS NULL, '', CONCAT('${process.env.Media_Server}', ${atchName}))`;
    };

    public static idsDecryption = (id : any) => {
        id = id.replaceAll(' ', '+');
        id = +(CryptoJS.AES.decrypt(id.trim(), '').toString(CryptoJS.enc.Utf8));
        return id;
    }; 
    public static dateByType = (type: string) => {
        if (type === Constants.PLAN_TYPE.Monthly) {
            const date = moment().add(1, 'months').format(Constants.DATE_FORMAT);
            return this.dateUtil.toEpoch(date);
        } else if (type === Constants.PLAN_TYPE.Yearly) {
            const date = moment().add(1, 'years').format(Constants.DATE_FORMAT);
            return this.dateUtil.toEpoch(date);
        }
    };

    public static addDateByType = (type: string , expireDate : number) => {
        if (type === Constants.PLAN_TYPE.Monthly) {
            const date = moment(expireDate).add(1, 'months').format(Constants.DATE_FORMAT);
            return this.dateUtil.toEpoch(date);
        } else if (type === Constants.PLAN_TYPE.Yearly) {
            const date = moment(expireDate).add(1, 'years').format(Constants.DATE_FORMAT);
            return this.dateUtil.toEpoch(date);
        }
    };

    public static async generateReferralCode(length: number = Constants.REFERRAL_CODE_LEN) {
        let code = '';
        const characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            code += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        const isReferralCodeUsed : any = await User.findOne({attributes : ['user_id'] , where :  { referral_code : code }});
        if (isReferralCodeUsed && isReferralCodeUsed.user_id) {
            await this.generateReferralCode();
        } else {
            return code;
        }
    }

    public static getWalletBalance = async (userId: number) => {
        const { wallet } : any = await User.findOne({attributes : ['wallet'] , where : { user_id : userId }});
        return +wallet;
    };
    public static getTotalAmountByType(type: string, totalEmployee: number, price: number) {
        const result = type === Constants.PLAN_TYPE.Yearly ? _.round(_.multiply(+totalEmployee * Constants.MONTH_IN_YEAR, +price), 2) : _.round(_.multiply(+totalEmployee, +price), 2);
        return result;
    }

    public static getTwoDecimalValue = (value: any) => {
        return _.round(value, 2);
    };
    
    public static getDiscountAmount = (referralAmount: any, newAmount: any) => {
        const discountOn = referralAmount < newAmount ? referralAmount : newAmount;
        return (discountOn * Constants.DEFAULT_COUNT_PER) / 100;
    };
}
