import * as nodemailer from 'nodemailer';

export class SendEmail {
    public static sendRawMail = (
        email: any = null,
        subject: any = null,
        text: any = null
    ) => {
        const host = process.env.SMTP_HOST;

        const transporter = nodemailer.createTransport({
            host,
            auth: {
                user: process.env.SMTP_USER_NAME,
                pass: process.env.SMTP_PASSWORD,
            },
        });

        const mailOptions = {
            from: process.env.DEFAULT_FROM,
            to: email,
            subject,
            text,
        };

        try{
            transporter.sendMail(mailOptions, (mailSendErr: any, info: any) => {
                console.log(
                    'Send Row Email : Output ' + JSON.stringify(mailSendErr)
                );
                if (!mailSendErr) {
                    console.log(`Message sent: ${info.response}`);
                } else {
                    console.log(
                        `Error in sending email: ${mailSendErr} and info ${info}`
                    );
                }
            });
        } catch(error) {
            console.log(error);
        }
    };
}
