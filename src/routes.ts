import * as express from 'express';
import * as l10n from 'jm-ez-l10n';
import { Constants } from './helpers/config/constants';
import { ProfileRoute } from './modules/profile/profile-routes';
export class Routes {
    protected basePath: any;

    constructor(NODE_ENV: string) {
        switch (NODE_ENV) {
        case 'production':
            this.basePath = '/app/dist';
            break;
        case 'development':
            this.basePath = '/app/public';
            break;
        }
    }

    public path() {
        const router = express.Router();

        router.use('/profile', ProfileRoute);
        router.all('/*', (req, res) => {
            return res.status(Constants.NOT_FOUND_CODE).json({
                error: l10n.t('ERR_URL_NOT_FOUND'),
            });
        });
        return router;
    }
}
