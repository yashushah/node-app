import sequelize from '../connection';
import User from './users';
import BillingDetails  from './billing-info';


User.build();
BillingDetails.build();

export default sequelize;