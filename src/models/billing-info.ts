import { INTEGER, STRING } from 'sequelize';
import { Tables } from '../helpers/config/tables';
import sequelize from '../connection';
import User from './users';

export class BillingDetailsSchema {
    public static billingDetails = {
        billing_id: {
            type: INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        user_id: {
            type: INTEGER,
            allowNull: false
        },
        company_name: {
            type:STRING
        },
        company_address:{
            type:STRING
        },
        mobile_number:{
            type:STRING
        },
        isd_code:{
            type:STRING
        },
        email:{
            type:STRING
        },
        gst:{
            type:STRING
        },
        pincode : {
            type:INTEGER
        }
    };
}

const BillingDetails = sequelize.define(Tables.BILLING_DETAILS, BillingDetailsSchema.billingDetails);

User.hasMany(BillingDetails,{foreignKey:'user_id'});
BillingDetails.belongsTo(User,{foreignKey:'user_id'});

export default BillingDetails;