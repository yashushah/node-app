const Joi = require('joi');

export class SchemaValidator {

    //update profile details schema
    public updateProfileSchema = Joi.object({
        first_name: Joi.string(),
        last_name: Joi.string(),
        mobile_number: Joi.number(),
        isd_code: Joi.string(),
        id: Joi.number().required(),
    });

    // change password schema
    public changePassword = Joi.object({
        password: Joi.string().required(),
        new_password: Joi.string().required(),
        id: Joi.number().required(),
    });

    // get user details schema
    public getUserDetailsSchema = Joi.object({
        id: Joi.number().required()
    });
}