import { Request, Response } from 'express';
import { ResponseBuilder } from '../../helpers/responseBuilder';
import { UserUtils } from './profile-utils';
import {Utils} from '../../helpers/utils';
import { Constants } from '../../helpers/config/constants';
export class UserController {
    private userUtils: UserUtils = new UserUtils();
    private utils = new Utils();
    // update profile controller
    public updateProfile = async (req: any, res: Response) => {
        const imagePath = await  Utils.getUploadedMediaId(req.files?.image,Constants.FILE_TYPES.IMAGE,'profile',req.body.image);
        const result: ResponseBuilder = await this.userUtils.updateProfile(req.body,req.body.id,imagePath);
        return res.status(result.code).json({message: result.msg});      
    };

    // change/update password controller
    public resetPassword = async (req: Request, res: Response) => {
        const result: ResponseBuilder = await this.userUtils.changePassword(req.body.data, req.body.id);
        return res.status(result.code).json({message: result.msg});        
    };

    // user details controller
    public getUserData = async (req: Request, res: Response) => {
        const result: ResponseBuilder = await this.userUtils.getUserData(req.params.id);
        if(result.code === Constants.SUCCESS_CODE) {
            return res.status(result.code).json(result.result);
        }
        return res.status(result.code).json({message: result.msg});
    };

    public editBillingInfo = async(req:Request,res:Response) => {
        const data = req.body.data;
        data.user_id =  data.user_id == undefined ? 0 : Utils.idsDecryption(data.user_id);
        const result = await this.userUtils.editBillingInfo(data);
        return res.status(result.code).json(result.msg);
    };

    public getBillingDetails = async(req:Request,res:Response) => {
        let user_id = req.query.user_id;
        user_id =  user_id == undefined ? 0 : Utils.idsDecryption(user_id);
        const result = await this.userUtils.getBillingDetails(user_id);
        return res.status(result.code).json(result);
    };

}