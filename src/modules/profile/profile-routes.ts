import { Router } from 'express';
import { UserController } from './profile-controller';
import { UserSchemaValidator } from './profile-validator';
const router: Router = Router();
const userController = new UserController();
const userSchemaValidator = new UserSchemaValidator();

/**
 * @swagger
 * /profile/update-profile:
 *  put:
 *      summary: Update Profile
 *      description: This api is use to update User Profile in web/app.
 *      requestBody :
 *          required : true
 *          content :
 *              application/json :
 *                  schema :
 *                      $ref: '#/components/schemas/updateProfile'
 *      responses:
 *          '200':
 *              description: User Profile update Successfully
 *              type: application/json
 */
router.put('/update-profile', userController.updateProfile);
/**
 * @swagger
 * /profile/reset-password:
 *  put:
 *      summary: Reset Password
 *      description: This api is use to Reset User Password in web/app.
 *      requestBody :
 *          required : true
 *          content :
 *              application/json :
 *                  schema :
 *                      $ref: '#/components/schemas/resetPassword'
 *      responses:
 *          '200':
 *              description: Reset Password Successfully
 *              type: application/json
 */
router.put('/reset-password', userSchemaValidator.changePassword, userController.resetPassword);
/**
 * @swagger
 * /profile/user-details/{id}:
 *  get:
 *      summary: Get user details
 *      description: This api is use to Get User details.
 *      parameters:
 *          - name : id
 *            in : path
 *            description : id of user
 *            required : true
 *            schema :
 *                type : number
 *      responses:
 *          '200':
 *              description: Successful-operation
 *              type: application/json
 */
router.get('/user-details/:id', userSchemaValidator.getUserDetails, userController.getUserData);

router.post('/billingDetails',userController.editBillingInfo);

router.get('/billingDetails',userController.getBillingDetails);

export const ProfileRoute: Router = router;

/**
 * @swagger
 * components:
 *  schemas:
 *      resetPassword:
 *          type : object
 *          properties :
 *              data :
 *                  type : object
 *                  properties : 
 *                      password : 
 *                          type : string
 *                      new_password : 
 *                          type : string
 *              id : 
 *                  type : number
 *      updateProfile:
 *          type : object
 *          properties :
 *              data :
 *                  type : object
 *                  properties : 
 *                      first_name : 
 *                          type : string
 *                      last_name : 
 *                          type : string
 *                      mobile_number : 
 *                          type : number
 *                      image_url : 
 *                          type : string
 *              id : 
 *                  type : number
 *      updateProfileRes:
 *          type : object
 *          properties :
 *              data :
 *                  type: object
 */
