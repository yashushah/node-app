import { Constants } from '../../helpers/config/constants';
import { SchemaValidator } from './profile-schema-validator';
import * as l10n from 'jm-ez-l10n';

export class UserSchemaValidator {
    private schemaValidator = new SchemaValidator();

    //update profile schema validation
    public updateProfile = async (req, res, next) => {
        const {id,first_name, last_name, mobile_number, isd_code} = req.body;
        const payload = {
            first_name,
            last_name,
            mobile_number,  
            isd_code,
            id
        };
        const { error } = this.schemaValidator.updateProfileSchema.validate(payload);
        if(error) {
            res.status(406);
            return res.json({message: error.message});
        } else {
            next();
        }
    };

    //change/update password schema validation
    public changePassword = async (req, res, next) => {
        const { password, new_password} = req.body.data;
        const id = req.body.id;
        const payload = {
            password,
            new_password,
            id
        };
        if(payload.new_password) {
            const passwordRegex = Constants.REGEX.PASSWORD_PATTERN;
            const validPass = passwordRegex.test(payload.new_password);
            if(!validPass) {
                res.status(406);
                return res.json({message:l10n.t('INCORRECT_PASSWORD_PATTERN')});
            }
        }
        const { error } = this.schemaValidator.changePassword.validate(payload);
        if(error) {
            res.status(406);
            return res.json({message: error.message});
        } else {
            next();
        }
    };

    // get user details schema validation
    public getUserDetails = async (req, res, next) => {
        const id = req.params.id;
        const payload = {
            id
        };
        const { error } = this.schemaValidator.getUserDetailsSchema.validate(payload);
        if(error) {
            res.status(406);
            return res.json({message: error.message});
        } else {
            next();
        }
    };
}
