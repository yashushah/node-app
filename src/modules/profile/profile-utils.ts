import { ResponseBuilder } from '../../helpers/responseBuilder';
import User from '../../models/users';
import * as l10n from 'jm-ez-l10n';
const bcrypt = require('bcrypt');
import {Aws} from '../../helpers/MediaUploadFiles/aws';
import BillingDetails from '../../models/billing-info';

export class UserUtils {
    private a = new Aws();
    
    public async updateProfile(data,id,imagePath): Promise<ResponseBuilder> {
        
        const userDetails = await User.findOne({where:{user_id : id}});
        if(userDetails) {
            const userStatus = userDetails['status'];
            if(userStatus !== 'active') {
                return ResponseBuilder.validation(l10n.t('ACCOUNT_NOT_ACTIVE'));
            } 
            else {
                if(imagePath?.dataPath){
                    data.profile_image = imagePath?.dataPath;
                }
                try{
                    const updatedEmail = await User.update(data,{where: {user_id: id}}).then(()=>true).catch(()=>false);
                    if(updatedEmail){
                        return ResponseBuilder.successMessage(l10n.t('USER_DETAIL_UPDATE'));
                    } else {
                        return ResponseBuilder.validation(l10n.t('ERR_UPDATE_PROFILE'));
                    }
                }catch(error) {
                    throw ResponseBuilder.error(error);
                }
            }
        } else {
            return ResponseBuilder.notFound(l10n.t('ERR_NO_USER_FOUND'));
        }
    }

    // change/update password
    public async changePassword(data, id):Promise<ResponseBuilder> {
        const userDetails = await User.findOne({where: {user_id: id}});
        if(userDetails) {
            const hashPassword = userDetails['password'];
            const isValidPassword = await bcrypt.compare(data.password, hashPassword);
            if(isValidPassword) {
                const newPassword = await bcrypt.hash(data.new_password, Number(10));
                const epchoTime = (new Date).getTime();
                const isUpdated = await User.update({password: newPassword,last_update_password:epchoTime}, {where : {user_id: id}}).then(()=>true).catch(()=>false);
                if(isUpdated) {
                    return ResponseBuilder.successMessage(l10n.t('SUCCESS'));
                } else {
                    return ResponseBuilder.errorMessage(l10n.t('FAILED'));
                }
            } else {
                return ResponseBuilder.unAuthorized(l10n.t('INCORRECT_PASS'));
            }
        }
    }

    // get user details 
    public async getUserData(id): Promise<ResponseBuilder> {
        const userDetails = await User.findOne({attributes:['first_name','last_name','mobile_number','email','isd_code','last_update_password','profile_image'], where: {user_id: id}});
        const result:any = {};
        if(userDetails) {
            const first_name = userDetails['first_name'];
            const last_name = userDetails['last_name'];
            const mobile_number = userDetails['mobile_number'];
            const email = userDetails['email'];
            const isd_code = userDetails['isd_code'];
            const last_update_password = userDetails['last_update_password'];
            result['first_name'] = first_name;
            result['last_name'] = last_name;
            result['mobile_number'] = mobile_number;
            result['email'] = email;
            result['isd_code'] = isd_code;
            result['last_update_password']=last_update_password;
            if(userDetails['profile_image']!=null){
                result['profile_image'] = userDetails['profile_image']?.length==7?userDetails['profile_image']:`${process.env.Media_Server}${userDetails['profile_image']}`;
            }
            return ResponseBuilder.data(result);
        } else {
            return ResponseBuilder.notFound(l10n.t('ERR_NO_USER_FOUND'));
        }
    }


    public async editBillingInfo(data) : Promise<ResponseBuilder>{
        if(!data.mobile_number.length){
            data.isd_code = null;
            data.mobile_number = null;
        }
        if(data.billing_id){
            const billing = await BillingDetails.update(data,{where:{
                billing_id:data.billing_id
            }});
            if(billing){
                return ResponseBuilder.successMessage(l10n.t('BILLING_DETAIL_UPDATED'));
            }
            return ResponseBuilder.successMessage(l10n.t('BILLING_DETAIL_UPDATED_FAILED'));
        }
        else{
            const billing = await BillingDetails.create(data).then(()=>true).catch((err)=>false);
            if(billing){
                return ResponseBuilder.successMessage(l10n.t('BILLING_DETAIL_UPDATED'));
            }
            return ResponseBuilder.successMessage(l10n.t('BILLING_DETAIL_UPDATED_FAILED'));
        }
    }

    public async getBillingDetails(userId) : Promise<ResponseBuilder>{
        const billingDetails:any = await BillingDetails.findOne({attributes:['billing_id','company_name','company_address','isd_code','mobile_number','email','gst'],where:{user_id:userId}});
        if(billingDetails && !billingDetails?.mobile_number){
            billingDetails.mobile_number=null;
            billingDetails.isd_code=null;
        }
        return ResponseBuilder.data(billingDetails);
    }
}